﻿# -*- coding: utf-8 -*-
"""
This is the function module. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL
from importlib import reload

import os
import sys
import tkinter as tk

import numpy as np
import cv2
import imutils
import abel

from inspect import getsourcefile
from collections import namedtuple as nt

import tifffile as tiff # ! DOC: https://www.lfd.uci.edu/~gohlke/code/tifffile.py.html

# EXTEND PATH
# ..

# INTERNAL
from constants import *   # !! import all global constants from LIB/constants.py

import formats as formats # !! import global formats from LIB/formats.py
reload(formats)

import functions as func  # !! functional routines applicable to many cases
reload(func)

import plotter as plot
reload(plot)

# STACKOVERFLOW
from s33293804 import *

# ==============================================================================
# CONSTANTS USED IN FUNCTIONS
# ==============================================================================
# HELPER PARAMETERS
global_list = []
global_bool = False

# METHODOLOGY SELECTORS
# define method used to load images, 
# possible values are
# opencv   :: use the module opencv
# tifffile :: use the module tifffile
global_load_method = 'opencv'

# define Abelization strategy
# possible values are
# three_point :: use three point algorithm
# hansenlaw   :: use Hansen-Law method
global_abel_strategy = 'three_point'

# CONSTANTS
global_avoid_underflow = False
global_fill_screen_percent = 0.9

# ==============================================================================
# FUNCTIONS TO MANAGE RUNTIME ENVIRONMENT
# ==============================================================================

def ISIMG(path):
    try:
        extension = func.GIVE_EXTENSION(path)
        if extension in formats.acceptedinput[global_load_method]:
            return True
        else:
            return False
    except:
        func.ERROR_PRINT(1287)
        
# ==============================================================================
# FUNCTIONS FOR I/O
# ==============================================================================

def IMREAD(path):
    if global_load_method == 'opencv':
        return cv2.imread(path,cv2.IMREAD_UNCHANGED)
    elif global_load_method == 'tifffile':
        return tiff.imread(os.path.abspath(path))
    else:
        func.ERROR_PRINT(1169)
        
def IMSHOW(img, *args, **kwargs):
    name = kwargs.get('name', "ANONYMOUS")
    
    # Routine
    print(' > ENTER IMAGE\n\n**********')
    print('ZOOM IN  BY HOLDING RIGHT MOUSE KEY AND MOVE UP')
    print('ZOOM OUT BY HOLDING RIGHT MOUSE KEY AND MOVE DOWN')
    print('EXIT BY WINDOW-X, q or esc')
    
    window = PanZoomWindow(img, name)
    
    key = -1
    
    while key != ord('q') and key != 27 and cv2.getWindowProperty(window.WINDOW_NAME, 0) >= 0:
        key = cv2.waitKey(5) #User can press 'q' or 'ESC' to exit

    cv2.destroyAllWindows()
    print('**********\n')
 
def IMWRITE(full_name, img):
    try:
        #extension = func.GIVE_EXTENSION(full_name)
        #if extension == 'tiff' or extension == 'tif':
        #    tiff.imsave(full_name, img)
        #else:
        if not cv2.imwrite(full_name, img):
            func.MESSAGE_PRINT('image.py','Issue with "'+full_name.replace(os.path.sep,'/')+'"','Write out to shell directory instead.')
            if not cv2.imwrite(os.path.split(full_name)[1], img):
                func.MESSAGE_PRINT('image.py','Shell directory protected!')
                func.ERROR_PRINT(161)
        return True
    except:
        func.ERROR_PRINT(13)
        
# ==============================================================================
# FUNCTIONS TO MODIFY IMAGES
# ==============================================================================
        
def REDUCEDPI(images,factor,*args, **kwargs):
    keys = kwargs.get('keys', ["wumwum"])
    output = {}
    for key in keys:
        if key != "wumwum":
            oldshape = images[key].shape
            newshape = (np.floor(oldshape[0]/factor).astype(int),np.floor(oldshape[1]/factor).astype(int))
            reduced = np.zeros(newshape,dtype=images[key].dtype)
            for i in range(newshape[0]):
                for j in range(newshape[1]):
                    value = 0.
                    for m in range(factor):
                        for n in range(factor):
                            value = value + images[key][i * factor + m, j * factor + n]
                    reduced[i,j] = value / float(factor**2)
            output[key] = reduced
        else:
            print("  IGNORE KEY 'wumwum'")
    return output
    
def REDUCEDIMENSIONS(img,selector):
    # !TODO np.average supports weights, usefull for option 'A'
    try:
        if selector == 'R':
            return img[:,:,1]
        elif selector == 'G':
            return img[:,:,2]
        elif selector == 'B':
            return img[:,:,3]
        elif selector == 'A':
            return np.average(img,axis=-1)
        else:
            func.ERROR_PRINT(1169)
    except:
        func.ERROR_PRINT(5)
        
def TO_RGB(img):
    # get color
    try:
        img = cv2.cvtColor(img,cv2.COLOR_GRAY2RGB)
    except:
        func.MESSAGE_PRINT('image.py',"Image can not be transformed via GRAY2RGB.")
    return img
    
def TO_8bit(img):
    return ((img-np.nanmin(img))/(np.nanmax(img)-np.nanmin(img))*(2**8-1)).astype("uint8")
        
def ENHANCED_VISIBILITY(img):
    # get color
    img = TO_RGB(img)
    # adjust dynamic range
    img = TO_8bit(img)
    # return in color scale
    return cv2.applyColorMap(img, cv2.COLORMAP_JET)
    
def FIT_SCREEN(img):
    global global_fill_screen_percent
    # take into account the screen size to display image properly
    root = tk.Tk()
    root.update_idletasks()
    root.attributes('-fullscreen', True)
    root.state('iconic')
    geometry = root.winfo_geometry()
    root.destroy()
    wxh = geometry.split('+')[0]
    sw = float(wxh.split('x')[0])
    sh = float(wxh.split('x')[1])
    # scale to screen size
    scalefactor_h = np.floor(img.shape[0] / sh * global_fill_screen_percent ) + 1
    scalefactor_w = np.floor(img.shape[1] / sw * global_fill_screen_percent ) + 1
    scalefactor = int(max(scalefactor_h,scalefactor_w))
    img = img[::scalefactor,::scalefactor]
    return img, scalefactor
    
def CROSSHAIR(img):
    # draw a centre crosshair onto the screen
    h = int(img.shape[0])
    w = int(img.shape[1])
    hori = int(h/2.)
    vert = int(w/2.)
    stub = 0.05
    cv2.line(img, (0,hori), (int(stub*w),hori), (255, 255, 255), thickness=3)
    cv2.line(img, (int((1-stub)*w),hori), (w-1,hori), (255, 255, 255), thickness=3)
    cv2.line(img, (vert,0), (vert,int(stub*h)), (255, 255, 255), thickness=3)
    cv2.line(img, (vert,int((1-stub)*h)), (vert,h-1), (255, 255, 255), thickness=3)
    cv2.line(img, (0,hori), (int(stub*w),hori), (0, 0, 0), thickness=1)
    cv2.line(img, (int((1-stub)*w),hori), (w-1,hori), (0, 0, 0), thickness=1)
    cv2.line(img, (vert,0), (vert,int(stub*h)), (0, 0, 0), thickness=1)
    cv2.line(img, (vert,int((1-stub)*h)), (vert,h-1), (0, 0, 0), thickness=1)
    cv2.circle(img, (vert,hori), int(0.3*min(h,w)), (0, 0, 0), thickness=3)
    cv2.circle(img, (vert,hori), int(0.3*min(h,w)), (255, 255, 255), thickness=1)
    cv2.circle(img, (vert,hori), int(0.1*min(h,w)), (0, 0, 0), thickness=3)
    cv2.circle(img, (vert,hori), int(0.1*min(h,w)), (255, 255, 255), thickness=1)
    return img
        
# ==============================================================================
# FUNCTIONS TO MANAGE IMAGES
# ==============================================================================

def BITRANGE(dtype):
    if dtype == np.uint8:
        return (0,2**-1)
    elif dtype == np.uint16:
        return (0,2**16-1)
    elif dtype == np.uint32:
        return (0,2**32-1)
    elif dtype == np.float:
        return (-1.,1.)
    elif dtype == np.int8:
        return (-2**7,2**7-1)
    elif dtype == np.int16:
        return (-2**15,2**15-1)
    elif dtype == np.int32:
        return (-2**31,2**31-1)
    else:
        func.MESSAGE_PRINT('image.py',"Bit depth can not be understood.")
        func.ERROR_PRINT(13)
    
def INTERACTIVE_CROP(img, *args, **kwargs):
    # CHECK OPTIONAL PRIORITARIAN INPUT
    name = kwargs.get('name', "ANONYMOUS")
    mode = kwargs.get('mode', "RECTANGULAR") # TODO
    adjust = kwargs.get('adjust', None)
    # CHECK IF PATH OR IMAGE
    if isinstance(img,str):
        img = IMREAD(img)
    # CHECK IF OPTIMIZED DISPLAY AND FIT TO SCREEN
    if adjust != None:
        # adjust dynamic range
        # !TODO
        #img = ((img-np.nanmin(img))/(np.nanmax(img)-np.nanmin(img))*(2**8-1)).astype("uint8")
        # take into account the screen size to display image properly
        img, scalefactor = FIT_SCREEN(img)
    else:
        scalefactor = 1   
    # CROP AREA
    print(' > ENTER IMAGE\n\n**********')
    (x,y,w,h) = cv2.selectROI("ORIGINAL "+name+" - SELECT ROI BY MOUSE KLICK+HOVER AND PRESS ENTER", img)
    cropped = img[y:y+h , x:x+w]
    # VERIFY
    cv2.imshow('CROPPED '+name+" - PRESS ANY KEY TO CONTINUE",cropped)
    # HANDLE USER FEEDBACK
    cv2.waitKey(0)
    print("   PRESS ANY KEY TO CONTINUE")
    cv2.destroyAllWindows()
    print('**********\n')
    
    return {'mode':mode,'corner_x':x*scalefactor,'corner_y':y*scalefactor,'width':w*scalefactor,'height':h*scalefactor}

def INTERACTIVE_LINE(img, *args, **kwargs):
    global global_list, global_bool
    global_list = []
    global_bool = False
    # MOUSE ACTION
    def click_and_get(event, x, y, flags, param):
        '''
        HELPER FUNCTION: CLICK EVENT FUNCTION
        https://www.pyimagesearch.com/2015/03/09/capturing-mouse-click-events-with-python-and-opencv/
        '''
        # grab references to the global variables
        global global_list, global_bool
        # if the left mouse button was clicked, record the starting
        # (x, y) coordinates and set pathing
        if event == cv2.EVENT_LBUTTONDOWN:
            if len(global_list) < 1:
                global_list = [(x, y)]
            elif len(global_list) == 2 : # !! add only to remove in main, click exit strategy
                global_bool = True
            else:
                global_list.append((x, y))
                # draw a line in the open named image outside the function
                cv2.line(img, global_list[-1], global_list[-2], (255, 10, 10), 2)
                cv2.imshow(name, img)
        # check to see if the right mouse button was released
        elif event == cv2.EVENT_RBUTTONDOWN:
            # delet last point
            global_list.pop()

    # CHECK OPTIONAL PRIORITARIAN INPUT
    name = kwargs.get('name', "SELECT AXIS :: LEFT MOUSE BUTTON = NEW POINT; SELECT TWO AND CONFIRM BY CLICK")
    adjust = kwargs.get('adjust', None)
    # OPTIMIZE DISPLAY
    if adjust != None:
        # take into account the screen size to display image properly
        img, scalefactor = FIT_SCREEN(img)
    else:
        scalefactor = 1 
    # COPY FOR RESET
    clone = img.copy()
    img = clone.copy()
    # USER INTERACTION
    print(' > ENTER IMAGE\n\n**********')
    print("  .. PRESS LEFT   MOUSE BUTTON TO ADD NEW POINT")
    print("  .. PRESS RIGHT  MOUSE BUTTON TO DELETE LAST POINT")
    print("  .. PRESS R      KEY          TO RESET")
    print("  .. PRESS H      KEY          TO SELECT CENTRE-HORIZONTAL")
    print("  .. PRESS V      KEY          TO SELECT CENTRE-VERTICAL")
    print("  .. PRESS Q      KEY          TO QUIT")
    print("  .. PRESS C      KEY          TO CONFIRM SELECTION")
    # !TODO add options for purely horizontal or vertical
    # CANVAS
    cv2.namedWindow(name)
    cv2.setMouseCallback(name, click_and_get)
    
    # ACTIVITY
    # keep looping until the 'q' key is pressed
    while True:
        # display the image and wait for a keypress
        cv2.imshow(name, img)
        key = cv2.waitKey(1) & 0xFF
        if key == ord("r"):
            # if the 'r' key is pressed, reset the cropping region
            img = clone.copy()
            global_list = []
        if key == ord("h"):
            # if the 'h' key is pressed, select centre horizontal
            global_list = [(int(0.1*clone.shape[0]),int(clone.shape[0]/2.)),\
                           (int(0.9*clone.shape[0]),int(clone.shape[0]/2.))]
            global_bool = True
        if key == ord("v"):
            # if the 'v' key is pressed, select centre vertical
            global_list = [(int(clone.shape[1]/2.),int(0.1*clone.shape[1])),\
                           (int(clone.shape[1]/2.),int(0.9*clone.shape[1]))]
            global_bool = True
        elif key == ord("c"):
            # if the 'c' key is pressed, break from the loop
            break
        if global_bool:
            # if two points are selected everything is fine
            break
    
    # housekeeping
    if len(global_list) < 2:
        print("   YOU FAILED THE TASK: THE LINE SHOULD BE DEFINED BY TWO POINTS!")
        global_list = INTERACTIVE_LINE(img)
    
    cv2.destroyAllWindows()
    print('**********\n')
    
    return np.array(global_list)*scalefactor

def INTERACTIVE_PATH(img, *args, **kwargs):
    global global_list, global_bool
    global_list = []
    global_bool = False
    def click_and_get(event, x, y, flags, param):
        '''
        HELPER FUNCTION: CLICK EVENT FUNCTION
        https://www.pyimagesearch.com/2015/03/09/capturing-mouse-click-events-with-python-and-opencv/
        '''
        # grab references to the global variables
        global global_list, global_bool
        # if the left mouse button was clicked, record the starting
        # (x, y) coordinates and set pathing
        if event == cv2.EVENT_LBUTTONDOWN:
            if len(global_list) < 1:
                global_list = [(x, y)]
            else:
                global_list.append((x, y))
                # draw a line in the open named image outside the function
                cv2.line(img, global_list[-1], global_list[-2], (255, 255, 255), 2)
                cv2.imshow(name, img)
            global_bool = True
        # check to see if the left mouse button was released
        elif event == cv2.EVENT_RBUTTONDOWN:
            # delet last point
            global_list.pop()
            global_bool = False
    # CHECK OPTIONAL PRIORITARIAN INPUT
    name = kwargs.get('name', "ANONYMOUS")
    dpi = kwargs.get('dpi', 6000)
    # SHIFT INFORMATION FOR INTERACTIVE POSITIONING
    shift_xy = [0,0]    
    # INFORMATION
    print("  ENTER PATHING ENVIRONMENT")
    choice = input("  type  M                to define path using the mouse\n"+\
                   "  type  path/to/file.npy to load from any file or\n"+\
                   "  type  A                to load from UTILS/path_um.npy\n"+\
                   "  press ENTER            to confirm:\n")
    
    if choice == ord("m"):
    
        # COPY FOR RESET
        clone = img.copy()
    
        print("  .. PRESS LEFT   MOUSE BUTTON TO ADD NEW POINT")
        print("  .. PRESS RIGHT  MOUSE BUTTON TO DELETE LAST POINT")
        print("  .. PRESS R      KEY          TO RESET")
        print("  .. PRESS Q      KEY          TO QUIT")
        print("  .. PRESS C      KEY          TO CONFIRM SELECTION")
        # CANVAS
        cv2.namedWindow(name)
        cv2.setMouseCallback(name, click_and_get)
        
        # ACTIVITY
        # keep looping until the 'q' key is pressed
        while True:
            # display the image and wait for a keypress
            cv2.imshow(name, img)
            key = cv2.waitKey(1) & 0xFF
            # if the 'r' key is pressed, reset the cropping region
            if key == ord("r"):
                img = clone.copy()
                global_list = []
            # if the 'c' key is pressed, break from the loop
            elif key == ord("c"):
                break
    
    else:
        
        try:
            # .. change to default if requested
            if choice.upper() == "A":
                choice = "UTILS/path_um.npy"
            # .. check and load
            if os.path.isfile(choice):
                path = np.load(choice)
                print("  LOADED FILE SPECIFIED")
                global_list = [(int(ti[0]/mm_to_um/inch_to_mm*dpi),\
                                int(ti[1]/mm_to_um/inch_to_mm*dpi)) for ti in path.tolist()]
            else:
                func.ERROR_PRINT(2)
        except:
            func.ERROR_PRINT(13)
            
        print("  .. PRESS R      KEY          TO SHIFT RIGHT")
        print("  .. PRESS L      KEY          TO SHIFT LEFT")
        print("  .. PRESS D      KEY          TO SHIFT DOWN")
        print("  .. PRESS U      KEY          TO SHIFT UP")
        print("  .. PRESS Q      KEY          TO QUIT")
        print("  .. PRESS C      KEY          TO CONFIRM SELECTION")
        
        # CANVAS
        cv2.namedWindow(name)
        
        max_x_path = int(np.nanmax(path[:,0])/mm_to_um/inch_to_mm*dpi)
        max_y_path = int(np.nanmax(path[:,1])/mm_to_um/inch_to_mm*dpi)
        
        max_x_img = img.shape[1]
        max_y_img = img.shape[0]
        
        black_img = np.zeros((max([max_y_img,max_y_path]),max([max_x_img,max_x_path]))).astype('uint8')
        
        black_img[:max_y_img,:max_x_img] = img
        
        # COPY FOR RESET
        clone = black_img.copy()
        cv2.imshow(name, black_img)
        
        # ACTIVITY
        # keep looping until the 'q' key is pressed
        while True:
            # display the image and wait for a keypress
            cv2.imshow(name, black_img)
            key = cv2.waitKey(1) & 0xFF
            # if the 'r' key is pressed, reset the cropping region
            if key == ord("r"):
                black_img = clone.copy()
                shift_xy[0] = shift_xy[0]+1
            elif key == ord("l"):
                black_img = clone.copy()
                shift_xy[0] = shift_xy[0]-1
            elif key == ord("u"):
                black_img = clone.copy()
                shift_xy[1] = shift_xy[1]-1
            elif key == ord("d"):
                black_img = clone.copy()
                shift_xy[1] = shift_xy[1]+1       
            # if the 'c' key is pressed, break from the loop
            elif key == ord("c"):
                break
            
            # .. draw conductor            
            for ip in range(len(global_list)-1):
                # .. draw line if all points are on canvas
                x_coord_0 = global_list[ip][0] + shift_xy[0]
                y_coord_0 = global_list[ip][1] + shift_xy[1]
                x_coord_1 = global_list[ip+1][0] + shift_xy[0]
                y_coord_1 = global_list[ip+1][1] + shift_xy[1]
                
                if x_coord_0 > 0 and y_coord_0 > 0 and \
                    x_coord_1 > 0 and y_coord_1 > 0 and\
                    x_coord_0 < black_img.shape[1] and y_coord_0 < black_img.shape[0] and\
                    x_coord_1 < black_img.shape[1] and y_coord_1 < black_img.shape[0]:
                    
                    cv2.line(black_img, (x_coord_1,y_coord_1), (x_coord_0,y_coord_0), (255, 255, 255), 2)

        # Generate shifted output coordinates
        x_shift = shift_xy[0]  # X-axis not inverted
        y_shift = shift_xy[1]  # Y-axis in images inverted INFORMATICS = - MATHEMATICS
        
        for ip in range(len(global_list)-1):
            global_list[ip] = (global_list[ip][0] + x_shift, global_list[ip][1] + y_shift)
    
    cv2.destroyAllWindows()    
    
    return global_list,shift_xy
    
# ==============================================================================
# FUNCTIONS TO CREATE IMAGES
# ==============================================================================

def GIVE_MASK(mode, shape, *args, **kwargs):
    """
    GIVES A BINARY MASK ARRAY
    MODE  :: TYPE OF MASK
    SHAPE :: 2D SHAPE OF OUTPUT ARRAY
    """
    # CHECK OPTIONAL PRIORITARIAN INPUT
    lower_value = kwargs.get('lower', 0)
    higher_value = kwargs.get('higher', 1)
    dia = kwargs.get('dia', '+') # !! accepts '+' for positive and '-' for negative
    dtype = kwargs.get('dtype', np.uint16)
    # GIVE MASK
    # invert
    if dia == "-":
        var = lower_value
        lower_value = higher_value
        higher_value = var
    # canvas
    mask = np.ones(shape, dtype=dtype) * lower_value
    # draw
    if mode == 'RECTANGULAR':
        corner_x = kwargs.get('corner_x', 0)
        corner_y = kwargs.get('corner_y', 0)
        w = kwargs.get('width', shape[1]-1)
        h = kwargs.get('height', shape[0]-1)
        mask[corner_y:corner_y+h,corner_x:corner_x+w] = higher_value
        #plot.imshow(mask[:,:,0])
        #plot.imshow(mask[:,:,1])
    else:
        func.ERROR_PRINT(1169)
    return mask
    
# ==============================================================================
# AUTOMATIC INTELLIGENCE
# ==============================================================================
    
def FIND_CIRCLES(img):
    # https://www.geeksforgeeks.org/circle-detection-using-opencv-python/
    if len(img.shape) == 3:
        # Convert to grayscale. 
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    elif len(img.shape) == 2:
        gray = img.astype(np.uint8)
    else:
        func.ERROR_PRINT(13)

    # Blur using 3 * 3 kernel. 
    gray_blurred = cv2.blur(gray, (3, 3)) 

    # Apply Hough transform on the blurred image. 
    detected_circles = cv2.HoughCircles(gray_blurred, cv2.HOUGH_GRADIENT, 1, 20, param1 = 50, param2 = 30, minRadius = 1, maxRadius = 40) 

    # Draw circles that are detected. 
    if detected_circles is not None: 

        # Convert the circle parameters a, b and r to integers. 
        detected_circles = np.uint16(np.around(detected_circles)) 

        for pt in detected_circles[0, :]: 
            a, b, r = pt[0], pt[1], pt[2] 

            # Draw the circumference of the circle. 
            cv2.circle(img, (a, b), r, (0, 255, 0), 2) 

            # Draw a small circle (of radius 1) to show the center. 
            cv2.circle(img, (a, b), 1, (0, 0, 255), 3) 
            cv2.imshow("Detected Circle", img) 
            cv2.waitKey(0)
            
def FIND_CORNERS(img):
    '''
    CORNER DETECTION
    https://docs.opencv.org/master/dc/d0d/tutorial_py_features_harris.html
    https://docs.opencv.org/master/d4/d8c/tutorial_py_shi_tomasi.html
    '''
    # get grayscale image
    try:
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    except:
        func.MESSAGE_PRINT('image.py','Can not parse RGB to grayscale.')
        gray = img.copy()
    # detect corners
    gray = np.float32(gray)
    dst = cv2.cornerHarris(gray,2,3,0.04)
    dst = cv2.dilate(dst,None)
    # avoid in place modifications
    img = TO_RGB(img)
    img[dst>0.1*dst.max()]=[0,0,255]
    return img
            
# ==============================================================================
# FUNCTIONS FOR MATH
# ==============================================================================

def FFT(img2D):
    global global_avoid_underflow
    # !TODO speed up https://docs.opencv.org/4.5.2/d8/d01/tutorial_discrete_fourier_transform.html
    # GIVE FAST FOURIER TRANSFORM
    # computed the 2-d discrete Fourier Transform
    dft = cv2.dft(img2D.astype(np.float32),flags = cv2.DFT_COMPLEX_OUTPUT)
    # shift the zero-frequency component to the center of the spectrum.
    dft_shift = np.fft.fftshift(dft)
    # CALCULATE SPECTRUM
    mag = cv2.magnitude(dft_shift[:,:,0],dft_shift[:,:,1])
    if global_avoid_underflow:
        mag_nonzero = np.nonzero(mag)
        min_mag = mag[mag_nonzero]
        elevated_mag = np.ones(mag.shape) * np.min(mag[mag_nonzero])
        elevated_mag[mag_nonzero] = mag[mag_nonzero]
        mag = elevated_mag
    # compute magnitude spectrum
    numrange = BITRANGE(img2D.dtype)
    c_norm = (numrange[1]-numrange[0])/np.log(1+np.nanmax(mag))
    magnitude_spectrum = c_norm*np.log(mag+1.) - numrange[0]
    # output
    #plot.fft(img2D,magnitude_spectrum)
    return dft_shift,magnitude_spectrum
    
def IFFT(ft2DinRandI, *args, **kwargs): 
    # CHECK OPTIONAL PRIORITARIAN INPUT
    mask = kwargs.get('mask', None)
    # GIVE INVERSE FAST FOURIER TRANSFORM
    if isinstance(mask, np.ndarray):
        fshift   = ft2DinRandI * mask
    else:
        fshift = ft2DinRandI
    f_ishift = np.fft.ifftshift(fshift)
    img_IFFT = cv2.idft(f_ishift)
    return img_IFFT,cv2.magnitude(img_IFFT[:,:,0],img_IFFT[:,:,1])
    
def abel_inversion(original,*args,**kwargs):
    '''
    Transform methods 2D -(cyl. sym.)> 3D, further reading e.g. in
    https://pyabel.readthedocs.io/en/latest/transform_methods.html
    https://pyabel.readthedocs.io/en/latest/transform_methods/comparison.html
    '''
    global global_abel_strategy
    # CHECK OPTIONAL PRIORITARIAN INPUT
    reference = kwargs.get('reference', None)
    visual = kwargs.get('visual', None)
    # CALC
    if isinstance(visual,np.ndarray):
        img = visual
    else:
        img = original.copy()
    if isinstance(reference,np.ndarray):
        img_ref = reference
    else:
        img_ref = np.zeros(original.shape)
    # find symmetry axis
    axis_xy = INTERACTIVE_LINE(img,adjust='T')
    delta_xy = axis_xy[1] - axis_xy[0]
    if delta_xy[0] == 0:
        horizontal = False
        angle = 0.
    elif delta_xy[1] == 0:
        horizontal = True
        angle = np.pi/2.
    else:
        angle = np.pi/2. - np.arctan(delta_xy[1]/delta_xy[0])
        horizontal = True

    # rotate image and original
    rotated = imutils.rotate_bound(img, angle * 360./(2.*np.pi) )
    rotated_org = imutils.rotate_bound(original, angle * 360./(2.*np.pi) )
    rotated_ref = imutils.rotate_bound(img_ref, angle * 360./(2.*np.pi) )
    # setup symmetry flag for Abel algo (due to bug in abel, always calculate vertical by prior image rotation)
    symmetry_axis = 0
    # calculate some centre coordinates
    x_frame = 0.
    if angle > 0. : x_frame = img.shape[0] * np.sin(angle)
    x_x = axis_xy[0][0] * np.cos(angle)
    x_y = - axis_xy[0][1] * np.sin(angle)
    
    y_frame = 0.
    if angle < 0. : y_frame = - img.shape[1] * np.sin(angle)
    y_x = axis_xy[0][0] * np.sin(angle)
    y_y = axis_xy[0][1] * np.cos(angle)
    
    x_c = int(x_frame + x_x + x_y)
    y_c = int(y_frame + y_x + y_y)

    # select ROI
    # cv2.circle(rotated, (x_c,y_c), radius=5, color=(255, 10, 10), thickness=-1)    
    if horizontal:
        # re-arrangement for display purpose, not to confuse user
        cropped = INTERACTIVE_CROP(imutils.rotate_bound(rotated,-90.),name='SELECT ROI',adjust='T')
        height = cropped['width']
        width = cropped['height']
        corner_x = rotated.shape[1] - cropped['corner_y'] - cropped['height']
        corner_y = cropped['corner_x']
        
        cropped['width'] = width
        cropped['height'] = height
        cropped['corner_x'] = corner_x
        cropped['corner_y'] = corner_y
    else:
        cropped = INTERACTIVE_CROP(rotated,name='SELECT ROI',adjust='T')

    # check if origin is in region of interest
    # ! TODO
    #if not func.point_in_polygon( [[cropped['corner_x'],cropped['corner_y']],\
    #                               [cropped['corner_x']+cropped['width'],cropped['corner_y']],\
    #                               [cropped['corner_x']+cropped['width'],cropped['corner_y']+cropped['height']],\
    #                               [cropped['corner_x'],cropped['corner_y']+cropped['height']],\
    #                              ], [x_c,y_c] ):
    #    func.MESSAGE_PRINT('image.py',"Origin not in ROI! Try Again..")
    #    return abel_inversion(img)

    # clip original and re-level reference subtraction to corner of cropped frame
    cropped_org = rotated_org[cropped['corner_y']:cropped['corner_y']+cropped['height'],\
                                    cropped['corner_x']:cropped['corner_x']+cropped['width']]
    cropped_ref = rotated_ref[cropped['corner_y']:cropped['corner_y']+cropped['height'],\
                                    cropped['corner_x']:cropped['corner_x']+cropped['width']]
    cropped_measurement = cropped_org + cropped_ref
    rel_zero_measurement = cropped_measurement[0,0]
    rel_zero_ref = cropped_ref[0,0]
    
    cropped_org = (cropped_measurement - rel_zero_measurement) - (cropped_ref - rel_zero_measurement)
    
    # run method
    # origin = (y_c-cropped['corner_y'],x_c-cropped['corner_x'])
    did_abel = ai_pyabel(cropped_org,\
                         symmetry_axis = symmetry_axis,\
                         method = global_abel_strategy )
    # prepare output
    if horizontal:
        did_abel = imutils.rotate_bound(did_abel,-90.)
    
    return did_abel
    
def ai_pyabel(myImage,*args,**kwargs):
    '''
    ABEL INVERSION BY pyABEL
    MYIMAGE       :: CARTESIAN INPUT IMAGE
    ORIGIN        :: IMAGE CENTRE
    SYMMETRY_AXIS :: VERTICAL OR HORIZONTAL RADIAL SYMMETRY
    
    ABEL INVERSION FOLLOWING THE THREE-POINT-METHOD
    [1] Dasch et al., Applied Optics, Vol 31, No 8, March 1992, Pg 1146-1152, note that
        some of the equations in the paper contained typos. These have been corrected, 
        as described in [2] the 2002 PhD thesis of 
    [2] Karl Matthew Martin, PhD Thesis: Acoustic Modification of Sooting Combustion, 
        University of Texas at Austin (2002), see pages 114, 115, and 170 in
        https://search.lib.utexas.edu/permalink/01UTAU_INST/be14ds/alma991007582549706011
    For this method to work, the real difference between adjacent projections should
     be much greater than the noise in the projections!
    
    ABEL INVERSION FOLLOWING THE HANSEN-LAW METHOD
    [1] E. W. Hansen, P.-L. Law, Recursive methods for computing the Abel transform and 
        its inverse, J. Opt. Soc. Am. A 2, 510–520 (1985)
    [2] E. W. Hansen, Fast Hankel transform algorithm, IEEE Trans. Acoust. Speech Signal 
        Proc. 33, 666–671 (1985)
    [3] J. R. Gascooke, PhD Thesis: Energy Transfer in Polyatomic-Rare Gas Collisions 
        and Van Der Waals Molecule Dissociation, Flinders University (2000)
    This method tends to perform better with images of large size n>1001 pixel width.
    '''
    # CHECK OPTIONAL PRIORITARIAN INPUT
    method = kwargs.get('method', 'three_point')
    if method == 'three_point' and myImage.shape[1] > 1001:
        func.MESSAGE_PRINT('image.py','Inverse Abel transform of wide images might be more efficient with the Hansen-Law strategy.')
    symmetry_axis = kwargs.get('symmetry_axis', None)
    if symmetry_axis == None:
        func.MESSAGE_PRINT('image.py','Symmetry axis for inverse Abel transform defaults to vertical.')
    # GIVE INVERSE ABEL TRANSFORM
    return abel.Transform(myImage, method=method, \
                                   direction='inverse', \
                                   origin = 'image_center', \
                                   symmetry_axis = symmetry_axis, \
                                   symmetrize_method = 'fourier', \
                                   use_quadrants = (True,True,True,True),\
                                   verbose = True).transform