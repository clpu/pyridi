﻿# -*- coding: utf-8 -*-
"""
This is the physics module. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL
from importlib import reload

import numpy as np

import skimage.restoration as ski_r
import scipy.integrate as sci_int
import cv2

# EXTEND PATH
# ..

# INTERNAL
from constants import *   # !! constants
import functions as func  # !! functional routines applicable to many cases
import image as image     # !! image routines applicable to many cases
import plotter as plotter # !! plotter routines applicable to many cases

# RELOAD
reload(func)
reload(image)
reload(plotter)

# ==============================================================================
# CONSTANTS USED IN FUNCTIONS
# ==============================================================================
# ..

# ==============================================================================
# HELPER FUNCTIONS
# ==============================================================================

def give_simple_interferogram(phase_difference_map,max_amplitude):
    return max_amplitude * 0.5 * (1. - np.cos(phase_difference_map))

# ==============================================================================
# FUNCTIONS TO MANAGE PHYSICS
# ==============================================================================

def angular_f(wavelength):
    """
    ANGULAR FREQUENCY
    WAVELENGTH :: WAVELENGTH OF LIGHT IN M
    
    RETURNS THE ANGULAR FREQUENCY IN s**-1
    """
    return 2.*np.pi / (wavelength/speed_of_light)

def n_c(wavelength):
    """
    CRITICAL DENSITY
    WAVELENGTH :: WAVELENGTH OF LIGHT IN M
    
    RETURNS THE CRITICAL DENSITY IN #/m**3
    """
    return epsilon_0 * m_e / q_e**2 * (angular_f(wavelength))**2
    
def phasediff_vac_neutralfluid(wavelength,neutral_density,path,substance,state):
    """
    PHASE DIFFERENCE FROM OPTICAL PATH DIFFERENCE TO VACUUM
     FOR A (STEP-WISE) CONSTANT PLASMA DENSITY
     delta_phi = 2pi/wavelength * integral (index(z) - 1) dz
     | A    := MR numberdensity / avogadro
     | index = sqrt( (2 A + 1 ) / ( 1 - A ) )
    WAVELENGTH     :: WAVELENGTH OF LIGHT IN M
    PLASMA_DENSITY :: PLASMA DENSITY IN M**-3
    
    RETURNS THE PHASE CHANGE IN rad
    
    DETAILS: Clausius-Mossotti-Lorenz-Lorentz
    
     Clausius-Mosotti relation as Lorentz–Lorenz equation 
     https://doi.org/10.1063/1.4719915
     | (index**2-1) / (index**2+2) = pi numberdensity mean_polarizability 4 / 3
     can be simplified to Gladstone-Dale relationship
     | index - 1 = constant * numberdensity
     and then used based on tabulated constants. Widely used, as in
     https://doi.org/10.1016/j.measurement.2004.04.003 .
     Alternatively, we deploy the concept of molar refractivity (MR)
     | MR = 4 pi avogadro mean_polarizability / 3
     | MR = avogadro polarizability / ( 3 epsilon0 )
     with unit [m3 mol-1]
     then following W. Foerst et.al. Chemie für Labor und Betrieb, 1967, 3, 32-34,
     https://organic-btc-ilmenau.jimdo.com/app/download/9062135220/molrefraktion.pdf?t=1616948905
     | (index**2-1) / (index**2+2) = MR numberdensity / avogadro
     to obtain
     | index = sqrt( (2 MR numberdensity / avogadro + 1 ) / ( 1 - MR numberdensity / avogadro ) )
     originally derived by Landolt-Ostwald-Bruehl
    """
    def integrand(neutral_density,critical_density):
        A = molar_refractivity(substance,state,wavelength) * cm_to_m**3 * neutral_density / avogadro
        return -1. + np.sqrt( (2. * A + 1. ) / ( 1. - A ) )
    integral = integrand(neutral_density,n_c(wavelength)) * path
    return 2.*np.pi/wavelength * integral
    # !TODO check equations, something is wrong in test case generation
    # !TODO mixes of fluids
    
def phasediff_vac_plasma(wavelength,plasma_density,path):
    """
    PHASE DIFFERENCE FROM OPTICAL PATH DIFFERENCE TO VACUUM
     FOR A (STEP-WISE) CONSTANT PLASMA DENSITY
     delta_phi = 2pi/wavelength * integral (index(z) - 1) dz
     | index_plasma = sqrt( 1 - n_e/n_c )
    WAVELENGTH     :: WAVELENGTH OF LIGHT IN M
    PLASMA_DENSITY :: PLASMA DENSITY IN M**-3
    
    RETURNS THE PHASE IN rad
    """
    def integrand(plasma_density,critical_density):
        return -1. + np.sqrt(1. - plasma_density/critical_density)
    #integral = sci_int.quad(integrand, lower_bound, upper_bound, args=(plasma_density,n_c(wavelength)))
    integral = integrand(plasma_density,n_c(wavelength)) * path
    return 2.*np.pi/wavelength * integral
    
def n_m_vac(wavelength,delta_phi_of_delta_r,substance,state):
    """
    MOLECULAR DENSITY DERIVED FROM OPTICAL PATH DIFFERENCE TO VACUUM
     delta_phi = 2pi/wavelength * integral (index(z) - 1) dz
     | A    := MR numberdensity / avogadro
     | index_neutral = sqrt( (2 A + 1 ) / ( 1 - A ) )
     | Abelization of coordinates
     delta_phi_of_delta_r(r) = 2pi/wavelength * ( index_neutral(r) - 1 )
     <=> (2 A + 1 ) / ( 1 - A ) = ( delta_phi_of_delta_r(r) wavelength / 2pi + 1 )**2 =: B
     <=> 2 A + 1 = B - B A
     <=> 2A + B A = B - 1
     <=> A = ( B - 1 ) / ( 2 + B )
    
    RETURNS THE MOLECULAR DENSITY IN #/m**3
    
    DETAILS
    https://doi.org/10.1063/1.4719915
     Clausius-Mosotti relation as Lorentz–Lorenz equation 
     | (index**2-1) / (index**2+2) = pi numberdensity mean_polarizability 4 / 3
     can be simplified to Gladstone-Dale relationship
     | index - 1 = constant * numberdensity
     and then used based on tabulated constants. Widely used, as in
     https://doi.org/10.1016/j.measurement.2004.04.003 .
     Alternatively, we deploy the concept of molar refractivity (MR)
     | MR = 4 pi avogadro mean_polarizability / 3
     | MR = avogadro polarizability / ( 3 epsilon0 )
     with unit [m3 mol-1]
     then following W. Foerst et.al. Chemie für Labor und Betrieb, 1967, 3, 32-34,
     https://organic-btc-ilmenau.jimdo.com/app/download/9062135220/molrefraktion.pdf?t=1616948905
     | (index**2-1) / (index**2+2) = MR numberdensity / avogadro
     to obtain
     | index = sqrt( (2 MR numberdensity / avogadro + 1 ) / ( 1 - MR numberdensity / avogadro ) )
     or for small (n-1) << 1
     | index = ...
    """
    infoplusonesquared = ( wavelength * delta_phi_of_delta_r / (2.*np.pi) + 1. )**2
    return - avogadro / molar_refractivity(substance,state,wavelength) * ( infoplusonesquared - 1. ) / ( infoplusonesquared + 2. ) * cm_to_m**(-3)
    
def n_e_vac(wavelength,delta_phi_of_delta_r):
    """
    ELECTRON DENSITY DERIVED FROM OPTICAL PATH DIFFERENCE TO VACUUM
     delta_phi = 2pi/wavelength * integral (index(z) - 1) dz
     | index_plasma = sqrt( 1 - n_e/n_c )
     | Abelization of coordinates
    WAVELENGTH :: WAVELENGTH OF LIGHT IN M
    
    RETURNS THE ELECTRON DENSITY IN #/m**3
    """
    return n_c(wavelength) * ( wavelength**2/(2.*np.pi) * delta_phi_of_delta_r**2 + \
                               wavelength/np.pi * delta_phi_of_delta_r )

# ==============================================================================
# FUNCTIONS TO MANAGE DATA
# ==============================================================================

def give_unwrapped_phase(interferogram, *args, **kwargs):
    '''
    Phase map retrival following
    A. Zaras-Szydlowska et al., Complex interferometry application forspontaneous
    magnetic field determination in laser-produced plasma, 44th EPS Conference on 
    Plasma Physics, P5.214, 2017, http://ocs.ciemat.es/EPS2017PAP/pdf/P5.214.pdf
    '''
    silent = kwargs.get('silent',False)
    FFT_highpass_mask = kwargs.get('FFT_highpass_mask',None)
    # prepare outputs
    data_dict = {}
    # 2D Discrete Fourier Transform via Fast Fourier Transform in opencv
    data_dict['FFT'],data_dict['FFT_spectrum'] = image.FFT(interferogram['image'])
    # Highpass Filter
    if FFT_highpass_mask is not None:
        data_dict['FFT_highpass_mask'] = FFT_highpass_mask
    else:
        # draw guides for the eye that separate the quadrants (in place transform)
        data_dict['FFT_spectrum'] = image.ENHANCED_VISIBILITY(data_dict['FFT_spectrum'])
        data_dict['FFT_spectrum'] = image.CROSSHAIR(data_dict['FFT_spectrum'])
        # construct high pass filter by ROI select
        data_dict['FFT_highpass_region'] = image.INTERACTIVE_CROP(data_dict['FFT_spectrum'],name='SELECT HIGHPASS',adjust='T')
        data_dict['FFT_highpass_mask'] = image.GIVE_MASK(data_dict['FFT_highpass_region']['mode'],\
                                                   (interferogram['height'],interferogram['width'],2),\
                                                   corner_x = data_dict['FFT_highpass_region']['corner_x'],\
                                                   corner_y = data_dict['FFT_highpass_region']['corner_y'],\
                                                   width = data_dict['FFT_highpass_region']['width'],\
                                                   height = data_dict['FFT_highpass_region']['height'],\
                                                   dtype = np.uint8 \
                                                  )
    # inverse transform
    data_dict['FFT_IFFT'],data_dict['FFT_IFFT_MAGNITUDE'] = image.IFFT(data_dict['FFT'], mask = data_dict['FFT_highpass_mask'])
    if not silent : plotter.imcompare(interferogram['image'],\
                      data_dict['FFT_IFFT_MAGNITUDE'],\
                      name1 = "Input Image", \
                      name2 = "Magnitude: sqrt({Im**2+Re**2}(IFT(mask*DFT)))")
    # calculate phase map
    data_dict['phase'] = np.arctan(data_dict['FFT_IFFT'][:,:,1]/data_dict['FFT_IFFT'][:,:,0])
    if not silent : plotter.imcompare(interferogram['image'],\
                      data_dict['phase'],\
                      name1 = "Input Image", \
                      name2 = "Phase: atan({Im/Re}(IFT(mask*DFT)))")
    # warn of corners that yield discontinuities
    # !TODO
    if not silent : plotter.imshow(image.FIND_CORNERS(data_dict['phase']),name="Corners")
    # unwraps the signal by changing elements which have an absolute difference from 
    #  their predecessor of more than 2*pi
    data_dict['phase'] = ski_r.unwrap_phase(data_dict['phase']*2.)/2. # !! factor of 2 taking into account range of atan()
    if not silent : plotter.imcompare(interferogram['image'],\
                      give_simple_interferogram(data_dict['phase'],\
                                                np.nanmax(interferogram['image'])),\
                      name1 = "Input Image", \
                      name2 = "Reconstructed: cos(phase)")
    # elevate phase to stable reference values
    # !TODO user select this point
    data_dict['phase'] = data_dict['phase'] - data_dict['phase'][0,0]
    if not silent : plotter.imcompare(interferogram['image'],\
                      data_dict['phase'],\
                      name1 = "Input Image", \
                      name2 = "Unwrapped Phase")
    return data_dict