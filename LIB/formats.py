﻿# -*- coding: utf-8 -*-
"""
This is the constants module. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL
# ..

# EXTEND PATH
# ..

# INTERNAL
# ..

# RELOAD
# ..

# ==============================================================================
# DEFINE FORMATS
# ==============================================================================

acceptedinput = {
    'opencv' : ['bmp','dib',\
                'jpeg','jpg','jpe',\
                'jp2',\
                'png',\
                'webp',\
                'pbm','pgm','ppm','pxm','pnm',\
                'pfm',\
                'sr',\
                'tiff','tif',\
                'exr',\
                'hdr','pic'],
    'tifffile' : ['tiff','tif']
}