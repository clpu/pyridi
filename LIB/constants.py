﻿# -*- coding: utf-8 -*-
"""
This is the constants module. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL
from importlib import reload
from inspect import getsourcefile   # !! inquiry of script location

import os

import numpy as np

# EXTEND PATH
# ..

# INTERNAL
import functions as func  # !! functional routines applicable to many cases

# RELOAD
reload(func)

# ==============================================================================
# DEFINE VARIABLES
# ==============================================================================
# Molar Refractivity
global_molar_refractivity_libpath = '../LIB/molarrefractivity.csv'
global_molar_refractivity = {}
global_got_molar_refractivity = False

# ==============================================================================
# DEFINE CONSTANTS
# ==============================================================================
# scales
inch_to_um = 25400.
inch_to_mm = 25.4
inch_to_m  = 0.0254

nm_to_m = 1.e-9

um_to_m = 1.e-6

mm_to_um = 1000.
mm_to_m = 1.e-3

cm_to_m = 0.01

m_to_um = 1000000.
m_to_cm = 100.

# physics in SI units
mu_0 = 1.256637062e-6                 # !! vacuum permeability
epsilon_0 = 8.85418781e-12            # !! vacuum permittivity
avogadro = 6.02214076e23              # !! Avogadro constant
m_e = 9.10938370e-31                  # !! electron mass
q_e = 1.602176634e-19                 # !! elementary charge
speed_of_light = 299792458            # !! speed of light in vacuum

# ==============================================================================
# DEFINE CALLS TO CONSTANTS' LIB
# ==============================================================================
# Molar Refractivity 
def get_molar_refractivity():
    """
    LOAD MOLAR REFRACTIVITY DICTIONARY FROM LIBRARY
    
    CONTAINS THE AVAILABLE MOLAR REFRACTIVITIES IN UNITS OF CM3MOL-1
    """
    global global_molar_refractivity_libpath
    # Load the Library
    source_lib_from_here = global_molar_refractivity_libpath
    # Assemble path to Library
    this_script_in = os.path.dirname(os.path.abspath(getsourcefile(lambda:0)))
    path = func.there_from_here(source_lib_from_here,this_script_in)
    # Load values to dictionary
    molar_refractivity_dict = func.READ_CSV(path,dictcols=2, header = True)
    # Out
    return molar_refractivity_dict
    
def molar_refractivity(species,state,wavelength):
    """
    QUERY OF MOLAR REFRACTIVITY
    SPECIES     :: SUBSTANCE
    STATE       :: PHASE OF SUBSTANCE
    WAVELENGTH  :: WAVELENGTH OF INTEREST
    
    RETURNS THE MOLAR REFRACTIVITY CLOSE TO THE WAVELENGTH OF INTEREST IN UNITS OF M3MOL-1
    """
    global global_got_molar_refractivity, global_molar_refractivity, global_molar_refractivity_libpath
    # Check if library is already available in dictionary
    if global_got_molar_refractivity:
        # .. then give values (after check of availability)
        if species in global_molar_refractivity.keys():
            if state in global_molar_refractivity[species].keys():
                func.MESSAGE_PRINT('constants.py',\
                                   'The Molar Refractivity is currently set to the closest',\
                                   'available value for a given wavelength.')
                if len(global_molar_refractivity[species][state]['wavelengths']) > 1:
                    match_id, match_wavelength = func.find_nearest(global_molar_refractivity[species][state]['wavelengths'],\
                                                                   wavelength)
                    
                    return global_molar_refractivity[species][state]['MolarRefractivity_cm3mol-1'][match_id]
                else:
                    return global_molar_refractivity[species][state]['MolarRefractivity_cm3mol-1']
            else:
                func.MESSAGE_PRINT('constants.py',\
                                   'Can not find '+state+' for '+species+' in',\
                                   global_molar_refractivity_libpath)
        else:
            func.MESSAGE_PRINT('constants.py',\
                               'Can not find '+species+' in',\
                               global_molar_refractivity_libpath)
        func.ERROR_PRINT(1169)
    else:
        # .. if not, load dictionary and change global variables
        global_molar_refractivity = get_molar_refractivity()
        global_got_molar_refractivity = True
        # .. and prepare arrays with available wavelength for faster computing of later steps
        for i in global_molar_refractivity.keys():
            for j in global_molar_refractivity[i].keys():
                wavelength_array = global_molar_refractivity[i][j]['Wavelength_nm']
                if not isinstance(wavelength_array,list):
                    wavelength_array = [wavelength_array]
                for k,wavelength_k in enumerate(wavelength_array):
                    if wavelength_k == 'infinity':
                        wavelength_array[k] = np.inf
                    else:
                        try:
                            wavelength_array[k] = float(wavelength_array[k])
                        except:
                            func.ERROR_PRINT(13)
                global_molar_refractivity[i][j]['wavelengths'] = np.array(wavelength_array)
        # .. and proceed with new call
        return molar_refractivity(species,state,wavelength)