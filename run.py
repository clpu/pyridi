# -*- coding: utf-8 -*-
"""
USER INFORMATION TO BE FOUND IN THE FILE README.MD

project             pythonic refractive index diagnostics: interferometry
acronym             pyridi
created on          2021-08-03-10:21:00

@author             Micha (MEmx), CLPU, Villamayor, Spain
@moderator          Carlos Salgado (CS), CLPU, Villamayor, Spain
@updator            Micha (MEmx), CLPU, Villamayor, Spain
            
@contact            michael@michaelmaxx.de

interpreter         python > 3.7
version control     git
history             pyridi is inspired by a matlab code written by CS prior to 2020 at CLPU

requires explicitely {
  - opencv
  - tifffile
  - matplotlib
  - scipy
  - scikit-image
  - pyabel
  - imutils
}
  
which includes implicitely {
  - numpy
  - inspect
  - importlib
  - os
  - sys
  - time
}

input file {
  - CSV COLUMNS :: KEY, VALUE, HELP
  - CSV ROWS    :: ASSIGNMENTS {KEY => VALUE}
}

flags {
  - 
}

execute command: python run.py input_file.csv *args(flags :: {None,"-".string})

"""
# ==============================================================================
# IMPORT PYTHON MODULES
# ==============================================================================
# EXTERNAL
from importlib import reload        # !! management of self-made modules
from inspect import getsourcefile   # !! inquiry of script location

import os                           # !! methods of operating system
import sys                          # !! executions by operating system
import time                         # !! time and date

import numpy as np                  # !! relatively fast array operations
import cv2                          # !! operations on images

import readchar                     # !! allows to read key input of stdin

# EXTEND PATH
root = os.path.dirname(os.path.abspath(getsourcefile(lambda:0))) # !! get environment
sys.path.append(os.path.abspath(root+os.path.sep+'LIB'))         # !! add library directory
sys.path.append(os.path.abspath(root+os.path.sep+'SCR'))         # !! add script directory
sys.path.append(os.path.abspath(root+os.path.sep+'TEST'))        # !! add test directory

# INTERNAL
from constants import *          # !! global constants from LIB/constants.py

import formats as formats        # !! global formats from LIB/formats.py

import iandt as integration_test # !! self test from TEST/iandt.py

import functions as func         # !! general collection of functions from SCR/functions.py
import loader as load            # !! routines fetching input data from SCR/loader.py
import plotter as plotter        # !! plotter routines from SCR/plotter.py
import exporter as export        # !! routines for saving output data from SCR/exporter.py

import image as image            # !! treatment of graphical data from SCR/image.py

import physics as physics        # !! physics calculations from SCR/physics.py

# ==============================================================================
# SET UP RUNTIME ENVIRONMENT
# ==============================================================================
# RELOAD INTERNAL MODULES
reload(export)
reload(formats)
reload(func)
reload(image)
reload(integration_test)
reload(load)
reload(physics)
reload(plotter)

# SEARCH FOR COMMAND LINE INPUT
args = func.COMMAND_LINE(['absolute_path_to_inputfile', \
                          'flags'])

# TAKE TIME
debut = time.time()

# PROMPT GREETINGS
print('\nWELCOME TO pyRIDI')

# CLEARIFY RUN
script_path,work_path = func.ENVIRONMENT(root = root)

# ROOM FOR TESTS
# ..

# ==============================================================================
# LOAD DIAGNOSTIC'S SETUP
# ==============================================================================
# PARSE INPUT FILES
input_dict = load.parse_inputfile(args[1],[script_path,work_path])

# CLEARIFY OUTPUT DIRECTORY
if 'output_directory' not in input_dict.keys():
    input_dict['output_directory'] = root+os.path.sep+'usr/out'

# ==============================================================================
# BUILD OPTIONAL ENVIRONMENTS
# ==============================================================================
# TEST DATA
if 'test' in input_dict.keys():
    if input_dict['test'] == 'T':
        input_dict['test_phase_map'] = integration_test.prepare_test(root)
    
# STORAGE SPACE
if 'output_directory' in input_dict.keys():
    export.check_isdir(input_dict['output_directory'])
    
# TEMPORARY FILES
if 'temp_dir' not in input_dict.keys():
    input_dict['temp_dir'] = root+os.path.sep+"TMP"
    export.check_isdir(input_dict['temp_dir'])

# ==============================================================================
# LOAD INPUT
# ==============================================================================
# PARSE DATA FILES
if load.USE_INPUT_KEY('run.py','data',input_dict):
    if 'online' in input_dict.keys():
        if input_dict["online"] == 'T':
            old_data = load.parse_datafiles(input_dict['data'],[script_path,work_path])
            input_data = []
        else:
            old_data = []
            input_data = load.parse_datafiles(input_dict['data'],[script_path,work_path])
    else:
        input_dict["online"] = 'F'
        old_data = []
        input_data = load.parse_datafiles(input_dict['data'],[script_path,work_path])
    
# PARSE REFERENCE FILE
if load.USE_INPUT_KEY('run.py','reference',input_dict):
    reference_data = load.parse_reference(input_dict['reference'],[script_path,work_path])

# ==============================================================================
# CALIBRATION
# ==============================================================================
# ABSOLUTE COORDINATES
if 'TCC_reference' in input_dict:
    # load image
    # .. !TODO
    # dialogue: choose TCC on reference image
    # .. !TODO
    # set zero
    x_0_pix = 0
    y_0_pix = 0
else:
    # default zero
    x_0_pix = 0
    y_0_pix = 0
    
# SPATIAL CALIBRATION
if load.USE_INPUT_KEY('run.py','know_magnification',input_dict):
    # settle if magnification is known
    if input_dict['know_magnification'] == 'T':
        if load.TEST_INPUT_KEY('run.py','pixel_calib',input_dict):
            # GET PIXEL CALIBRATION IN um/pix
            pixel_calib = input_dict['pixel_calib']
        else:
            # CALCULATE SIZE OF IMAGED AREA
            pixel_calib = None
            # load magnification
            if load.USE_INPUT_KEY('run.py','magnification',input_dict):
                magnification = input_dict['magnification']
            # load detector size
            if load.USE_INPUT_KEY('run.py','detector_width',input_dict):
                detector_width = input_dict['detector_width']
            if load.USE_INPUT_KEY('run.py','detector_height',input_dict):
                detector_height = input_dict['detector_height']
            # derive horizontal size of imaged area
            x_size = detector_width / magnification * mm_to_m
            # derive vertical size of imaged area
            y_size = detector_height / magnification * mm_to_m
    elif input_dict['know_magnification'] == 'F':
        # determine size of imaged area
        # .. !TODO
        True
    else:
        func.MESSAGE_PRINT('run.py',"Unknown value for 'know_magnification'!")
        func.ERROR_PRINT(13)
else:
    func.MESSAGE_PRINT('run.py',"The input field for 'know_magnification' is empty!")
    func.ERROR_PRINT(1169)
    
# CAMERA REFERENCE
# electronic noise
# .. !TODO

# ==============================================================================
# ANALYZE DATA
# ==============================================================================
# MAKE SURE WAVELENGTH OF PROBING LIGHT IS KNOWN
if load.USE_INPUT_KEY('run.py','wavelength',input_dict):
    wavelength = input_dict['wavelength'] * nm_to_m

# EXTRACT REFERENCE PHASE MAP
# load image
if image.ISIMG(reference_data): 
    ref = load.dataset(reference_data)
    plotter.imshow(ref['image'],name="Reference Data")
else:
    func.MESSAGE_PRINT('run.py',"No valid reference file defined!")
    func.ERROR_PRINT(2)

# reduce complexity
if ref['channels'] > 1:
    if load.USE_INPUT_KEY('run.py','channel',input_dict):
        ref['image'] = image.REDUCEDIMENSIONS(ref['image'],input_dict['channel'])
        ref['channels'] = 1

# first physics calculation for reference: get unwrapped phase
ref['physics'] = physics.give_unwrapped_phase(ref)
the_active_mask = ref['physics']['FFT_highpass_mask']

# check for test case
if 'test_phase_map' in input_dict.keys():
    #plotter.phase(input_dict['test_phase_map']['ref'],ref['physics']['phase'])
    #in_inte = physics.give_simple_interferogram(input_dict['test_phase_map']['ref'],2.**8 - 1.)
    #out_inte = physics.give_simple_interferogram(ref['physics']['phase'],2.**8 - 1.)
    #plotter.phase(in_inte,out_inte)
    pass

# FORMALIZE ANALYSIS PROCEDURE
def analyze(input_file,input_dict, *args, **kwargs):
    global ref, x_size, y_size
    do_full_analysis = kwargs.get('full_analysis',True)
    FFT_highpass_mask = kwargs.get('FFT_highpass_mask',None)
    silent = kwargs.get('silent',False)
    # DIALOGUE
    # raise awareness
    print("\nANALYZE '"+input_file+"'")
    # load image
    if image.ISIMG(input_file): 
        img = load.dataset(input_file)
    else:
        return False
    
    # reduce complexity
    if img['channels'] > 1:
        if load.USE_INPUT_KEY('run.py','channel',input_dict):
            img['image'] = image.REDUCEDIMENSIONS(img['image'],input_dict['channel'])
            img['channels'] = 1
    
    # reduce load
    # .. !TODO
    # img = image.REDUCEDPI(img,reduce_dpi,keys = img.keys()) # imS = cv2.resize(im, (960, 540))
    
    # de-noise and filtering
    # .. !TODO
    
    # subtract electronic camera background
    # .. !TODO
    
    # first physics calculation for data: get unwrapped phase
    img['physics'] = physics.give_unwrapped_phase(img, silent = silent, FFT_highpass_mask = FFT_highpass_mask)
    
    # further physics calculation: get phase difference between data and reference
    img['physics']['delta_phase'] = img['physics']['phase'] - ref['physics']['phase']
    
    print(" > PLOT PHASE DIFFERENCE")
    plotter.colormap(img['physics']['delta_phase'],input_dict['temp_dir']+os.path.sep+"phasediff.png",x_label = "X / [pix]", y_label = "Y / [pix]", color_label="Phase Difference / [rad]", silent = True)
    
    if not do_full_analysis:
        if os.path.isfile(input_dict['temp_dir']+os.path.sep+"density.png"):
            os.remove(input_dict['temp_dir']+os.path.sep+"density.png")
        return False
    
    if not silent : plotter.imshow(img['physics']['delta_phase'],name="Global Phase difference")
    
    # Abelization of coordinates of phase difference
    img['physics']['abel_inversion'] = image.abel_inversion(img['physics']['delta_phase'], reference = ref['physics']['phase'],  visual = image.ENHANCED_VISIBILITY(img['image']))
    if not silent : plotter.imshow(img['physics']['abel_inversion'],name="Abel inverted phase map")
    
    # Electron Density
    if pixel_calib == None:
        pix_per_space_hori = img['width']/x_size
        pix_per_space_vert = img['height']/y_size
        # !TODO BEGIN
        # this presume squared pixel...
        pix_per_space = (pix_per_space_hori + pix_per_space_vert) / 2.
    else:
        pix_per_space = 1./(pixel_calib*um_to_m)
    # !TODO END
    # !TODO allow also analysis of neutral density gas in the following
    if load.USE_INPUT_KEY('run.py','ionization_state',input_dict):
        # settle if ionization state is known
        if input_dict['ionization_state'] == 0:
            # .. use refractive index of a neutral fluid
            img['physics']['molecular_density'] = physics.n_m_vac(wavelength,\
                                                                 img['physics']['abel_inversion'] * pix_per_space,\
                                                                 input_dict['substance'],\
                                                                 input_dict['substance_state'])
        else:
            # .. use refractive index of a plasma
            img['physics']['electron_density'] = physics.n_e_vac(wavelength,\
                                                                 img['physics']['abel_inversion'] * pix_per_space)
    else:
        func.MESSAGE_PRINT('run.py',"The input field for 'ionization_state' is empty!")
        func.ERROR_PRINT(1169)
    
    print(" > PLOT DERIVED DENSITY")
    if 'electron_density' in img['physics'].keys():
        plotter.colormap(img['physics']['electron_density']/m_to_cm**3,input_dict['temp_dir']+os.path.sep+"density.png",x_label = "X / [pix]", y_label = "Y / [pix]", color_label="Plasma Electron Density / [cm-3]", silent = True)
    elif 'molecular_density' in img['physics'].keys():
        plotter.colormap(img['physics']['molecular_density']/m_to_cm**3,input_dict['temp_dir']+os.path.sep+"density.png",x_label = "X / [pix]", y_label = "Y / [pix]", color_label="Neutral Molecular Density / [cm-3]", silent = True)
    else:
        func.ERROR_PRINT(1160)
    # return results
    return img
    
# FORMALIZE VISUALIZATION AND SAVING PROCEDURE
def write_out(img, *args, **kwargs):
    silent = kwargs.get('full_analysis',False)
    # visualize results
    if 'physics' in img.keys() and not silent:
        if load.TEST_INPUT_KEY('run.py','electron_density',img['physics']):
            plotter.mapping(img['physics']['electron_density'],name="Electron Density",clabel="m-3")
        elif load.TEST_INPUT_KEY('run.py','molecular_density',img['physics']):
            plotter.mapping(img['physics']['molecular_density'],name="Molecular Density",clabel="m-3")
        else:
            func.MESSAGE_PRINT('run.py',"DO NOT FIND RESULTS FOR PLOTTER!")
    else:
        func.MESSAGE_PRINT('run.py',"DO NOT FIND RESULTS FOR",img['source'])
    # save results
    
# LOOP OVER INPUT FILES
print('\nWORK WITH DATA\n')
if input_dict["online"] == 'F':
    # DATA ANALYSIS
    for file in input_data:
        # Call analysis procedure
        img = analyze(file,input_dict)
        if img == False:
            continue
        # Call output procedures
        write_out(img)
elif input_dict["online"] == "T":
    # ONLINE DATA ANALYSIS
    help_message =  'In order to exit the online analysis, press and hold ctrl + C\n'+\
                    'For phase differences only,           press and hold P\n'+\
                    'To release phase differences only,    press and hold O\n'+\
                    'All analysis with the last mask,      press and hold M\n'+\
                    'To release fixed mask mode,           press and hold N\n'+\
                    'To repeat the last analysis,          press and hold R\n'
    try:
        print("WAIT FOR NEW DATA")
        func.MESSAGE_PRINT('run.py',help_message)
        do_full_analysis = True
        use_last_mask = False
        while True:
            # get new data files
            input_data,old_data = load.next_datafiles(input_dict['data'],[script_path,work_path],old_data)
            # process data files if any
            for file in input_data:
                # call analysis procedure
                print('\n')
                if not use_last_mask:
                    img = analyze(file,input_dict,full_analysis = do_full_analysis, silent = True)
                    the_active_mask = img['physics']['FFT_highpass_mask']
                else:
                    img = analyze(file,input_dict,full_analysis = do_full_analysis, silent = True, FFT_highpass_mask = the_active_mask)
                print('\n')
                if img == False:
                    # remember file
                    old_data.append(file)
                    # go to next
                    continue
                # call output procedures
                write_out(img, silent = True)
                # remember file
                old_data.append(file)
                
            # wait and listen one second
            print('#', end="", flush=True)
            inp = func.FunctionThread(readchar.readkey)
            inp.start()
            time.sleep(1)
            if inp.result != None:
                if inp.result == "'r'" and len(old_data) >= 1:
                    old_data.pop()
                elif inp.result == "'p'":
                    print('\n\nANALYZE PHASE ONLY\n')
                    do_full_analysis = False
                elif inp.result == "'o'":
                    print('\n\nANALYZE ALL STEPS\n')
                    do_full_analysis = True
                elif inp.result == "'m'":
                    print('\n\nANALYZE WITH FIXED MASK\n')
                    use_last_mask = True
                elif inp.result == "'n'":
                    print('\n\nANALYZE WITH INDIVIDUAL MASKS\n')
                    use_last_mask = False
                elif inp.result == "'h'":
                    print('\n\n')
                    func.MESSAGE_PRINT('run.py',help_message)
                else:
                    print('\n\n')
                    func.MESSAGE_PRINT('run.py',inp.result+" is no valid input, for help press and hold H")
                    
            inp.raiseExc(Exception)
            del inp # play safe
            
    except (KeyboardInterrupt, SystemExit):
        func.MESSAGE_PRINT('run.py',"\nEXIT ONLINE ANALYSIS")   
    print("#")
else:
    func.ERROR_PRINT(1287)

# ==============================================================================
# TERMINATE
# ==============================================================================
# TAKE TIME
fin = time.time()

# PROMPT RUNTIME
print("\nRUNTIME = %3.1f s" % (fin-debut))

# SAY GOODBYE
func.GOODBYE()