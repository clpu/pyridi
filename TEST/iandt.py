﻿# -*- coding: utf-8 -*-
"""
This is the test module. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL
from importlib import reload

import os

import numpy as np

# EXTEND PATH
# ..

# INTERNAL
import image as image      # !! functional routines applicable to many cases

import plotter as plotter

import physics as physics  # !! physics routines applicable to many cases

# RELOAD
reload(image)
reload(plotter)
reload(physics)

# ==============================================================================
# CONSTANTS USED IN FUNCTIONS
# ==============================================================================
# canvas
global_shape = (600,800)
global_dtype = np.uint8

global_pixel_to_TCC_m = 1.e-6

# probe beam and target !TODO take from input file or vice versa
global_wavelength = 800.e-9    # !! m
global_plasmadensity = 1.e25   # !! m-3
global_gasdensity = 1.e27   # !! m-3

# ==============================================================================
# FUNCTIONS
# ==============================================================================
def prepare_test(path):
    global global_shape, global_dtype
    print('\nPREPARE INTEGRATION TEST')
    # reference phase map
    reference_phase = prepare_test_ref(global_shape,global_dtype)
    # save reference interferogram
    ref_data_path = path+os.path.sep+'TEST'+os.path.sep+"ref.tif"
    intensity = physics.give_simple_interferogram(reference_phase,2.**8 - 1.)
    image.IMWRITE(ref_data_path,intensity.astype(global_dtype))
    # perturbated phase by warm dense rod
    test_phase_wdr = prepare_wdr_test_phasechange(global_shape,global_dtype)
    # create interferogram
    intensity = physics.give_simple_interferogram(test_phase_wdr,2.**8 - 1.)
    # save interferogram
    test_data_path = path+os.path.sep+'TEST'+os.path.sep+'DATA'+os.path.sep+"warm_dense_rod.tiff"
    image.IMWRITE(test_data_path,intensity.astype(global_dtype))
    # perturbated phase by gaussian blob of plasma
    test_phase_pgb = prepare_pgb_test_phasechange(global_shape,global_dtype)
    # create interferogram
    intensity = physics.give_simple_interferogram(test_phase_pgb,2.**8 - 1.)
    # save interferogram
    test_data_path = path+os.path.sep+'TEST'+os.path.sep+'DATA'+os.path.sep+"gaussian_blob_plasma.tiff"
    image.IMWRITE(test_data_path,intensity.astype(global_dtype))
    # perturbated phase by gaussian blob of neutral gas
    test_phase_ngb = prepare_ngb_test_phasechange(global_shape,global_dtype)
    test_data_path = path+os.path.sep+'TEST'+os.path.sep+"gaussian_blob_neutral_phase.tiff"
    image.IMWRITE(test_data_path,test_phase_ngb.astype(global_dtype))
    # create interferogram
    intensity = physics.give_simple_interferogram(test_phase_ngb,2.**8 - 1.)
    # save interferogram
    test_data_path = path+os.path.sep+'TEST'+os.path.sep+'DATA'+os.path.sep+"gaussian_blob_neutral.tiff"
    image.IMWRITE(test_data_path,intensity.astype(global_dtype))
    # return test bed
    return {'warm_dense_wire':test_phase_wdr, 'gaussian_blob_plasma':test_phase_pgb, 'gaussian_blob_neutral':test_phase_ngb, 'ref':reference_phase}
    
def prepare_test_ref(shape,dtype):
    periods = 20.
    # vertical fringes
    y = np.linspace(0.,1.,shape[1])
    phase_map = y[np.newaxis,:] * np.ones(shape)
    return phase_map / np.nanmax(phase_map) * periods * 2. * np.pi

def prepare_wdr_test_phasechange(shape,dtype):
    global global_plasmadensity, global_wavelength, global_pixel_to_TCC_m
    # of type radial theta distribution
    radius = shape[1]/4. * global_pixel_to_TCC_m
    def pathlength(dr):
        if isinstance(dr, np.ndarray):
            return np.where(np.abs(dr) > radius, 0., 2. * np.sqrt(radius**2 - dr**2))
        else:
            return 2. * np.sqrt(radius**2 - dr**2)
            
    dr_array = np.linspace(-shape[1]/2.,shape[1]/2.,shape[1]) * global_pixel_to_TCC_m
    dl_array = np.real(pathlength(dr_array))
    # calculate phasshift
    dp = physics.phasediff_vac_plasma(global_wavelength,global_plasmadensity,dl_array)
    # call basic phase
    base_phase = prepare_test_ref(shape,dtype)
    # construct phasemap
    phase_map = base_phase + dp[np.newaxis,:] * np.ones(shape)
    # return
    return phase_map
    
def prepare_pgb_test_phasechange(shape,dtype):
    global global_plasmadensity, global_wavelength, global_pixel_to_TCC_m
    # scale factor for density
    scale = 30.
    # of type 3D gaussian worm of plasma
    radius = shape[1]/scale * global_pixel_to_TCC_m
    def gaussian_blob(radial_position):
        return np.exp(-(radial_position/radius/2.)**2)
    def phasedifference(dr):
        plotter.mapping(global_plasmadensity * gaussian_blob(dr)[np.newaxis,:]*np.ones(shape),name="Test Electron Density",clabel="m-3")
        # construct long line of sight, here 10 times the radius with point density comparable to frame
        line_of_sight = np.linspace(-scale*radius,scale*radius,int(scale)*shape[1])
        dl = 2.*radius/shape[1] # .. since scale falls out
        radials = np.sqrt((dr**2)[:,np.newaxis] + (line_of_sight**2)[np.newaxis,:])
        plasma_density_of_sight = global_plasmadensity * gaussian_blob(radials)
        step_phasediff = physics.phasediff_vac_plasma(global_wavelength,plasma_density_of_sight,dl) 
        return np.sum(step_phasediff, axis=1)
    dr_array = np.linspace(-shape[1]/2.,shape[1]/2.,shape[1]) * global_pixel_to_TCC_m
    # calculate phasshift
    dp = np.real(phasedifference(dr_array))
    # call basic phase
    base_phase = prepare_test_ref(shape,dtype)
    # construct phasemap
    phase_map = base_phase + dp[np.newaxis,:] * np.ones(shape)
    # return
    return phase_map
    
def prepare_ngb_test_phasechange(shape,dtype):
    global global_gasdensity, global_wavelength, global_pixel_to_TCC_m
    # scale factor for density
    scale = 30.
    # of type 3D gaussian worm of neutral nitrogen
    radius = shape[1]/scale* global_pixel_to_TCC_m
    def gaussian_blob(radial_position):
        return np.exp(-(radial_position/radius/2.)**2)
    def phasedifference(dr):
        plotter.mapping(global_gasdensity * gaussian_blob(dr)[np.newaxis,:]*np.ones(shape),name="Test Molecular Density",clabel="m-3")
        # construct long line of sight, here 10 times the radius with point density comparable to frame
        line_of_sight = np.linspace(-scale*radius,scale*radius,int(scale)*shape[1])
        dl = 2.*radius/shape[1] # .. since scale falls out
        radials = np.sqrt((dr**2)[:,np.newaxis] + (line_of_sight**2)[np.newaxis,:])
        molecular_density_of_sight = global_gasdensity * gaussian_blob(radials)
        step_phasediff = physics.phasediff_vac_neutralfluid(global_wavelength,molecular_density_of_sight,dl,'nitrogen','gas')
        return np.sum(step_phasediff, axis=1)
    dr_array = np.linspace(-shape[1]/2.,shape[1]/2.,shape[1]) * global_pixel_to_TCC_m
    # calculate phasshift
    dp = np.real(phasedifference(dr_array))
    # call basic phase
    base_phase = prepare_test_ref(shape,dtype)
    # construct phasemap
    phase_map = base_phase + dp[np.newaxis,:] * np.ones(shape)
    # return
    return phase_map