# Runtime Environment

The python script is best cloned from the source (not downloaded) and executed within an Anaconda environment (not within your standard python environment). The first is usefull for uncomplicated updates and an easy contribution to the project, the later is ought to protect your habitual working environments and keep them clean. The installation and execution procedures are kept simple in order to allow users to analyze data who are not familiar to python.

## GIT Environment

*pyRIDI* is a collaborative project, we would like to encourage that you get started with [git](https://git-scm.com/downloads) in order to join the developpers. Anyway, please use to following method to install the relevant scripts on your computer: 

1. make sure git is installed
  A. in Windows search for `gitbash` and install [git for windows](https://gitforwindows.org/) if you can not locate it, 
  B. in POSIX systems try `which git` and install [git](https://git-scm.com/downloads) if no answer is given to you; 
2. copy the project files from the internet onto your computer
  1. open *gitbash* or *terminal* respectively,
  2. navigate to a location that you want to use as working directory - e.g. in Windows `C:\working\directory` or in POSIX sustems `/working/directory`, 
  3. type `git clone https://memx@bitbucket.org/clpu/pyridi.git` and download the project files by executing with `enter` - here `https://memx@bitbucket.org/clpu/pyridi.git` is the default repository location which you can change if your institution has another version of the code on some server - or if you have a version of the code in a file;
3. if you open a filemanager, you can take a look in the created directory `pyridi` - it should be populated with some files and directories.

## Python Environment

We recommend you to set up a clean python environment with [Anaconda](https://www.anaconda.com/distribution/). Set up Anaconda (version 3) by following the installation guides on the project's web page. If you do not like to work with Anaconda, please find the details of used packages in `pyridi/pyridi.yml`. Depending on the operating system, the next setup steps differ.

### Windows
Open Anaconda navigator, navigate to *Environments* and click on the play button behind *base* to launch the anaconda prompt. Navigate to the `pyridi` directory, then execute the following installation instructions (answer with yes, if asked for permission to install packages)
```
(base) C:\working\directory\pyridi> conda env create -f pyridi.yml
(base) C:\working\directory\pyridi> conda activate pyridi
(pyridi) C:\working\directory\pyridi> 
```

### Linux
```
(base) user@computer:/working/directory/pyridi$ conda env create -f pyridi.yml
(base) user@computer:/working/directory/pyridi$ conda activate pyridi
(pyridi) user@computer:/working/directory/pyridi$
```

## Test Run

Perform a first run as test to ensure that the environment works fine. Execute
```
python run.py
```

## Known Errors and Troubleshooting

### OpenCV

Often the module *CV2* is not recognized, manifested with the error
```
    import cv2
ImportError: DLL load failed: The specified module could not be found.
```
then do
```
conda update -n base -c defaults conda
conda install -c fastai opencv-python-headless
conda uninstall opencv
conda uninstall opencv-python-headless
conda install opencv
```
which shakes a bit the faulty installation and eventually brings you CV2 with GUI support.

---

# Workflow

The script `pyridi/run.py` takes information from input files during its run. The test run makes use of the example input file `pyridi/input\_file.csv`. To get going, you can copy and modify this file to start with your own setup: e.g. the folder `pyridi/usr` is not used for project development and could be ideal to serve as your user directory. The comma separated input file contains one input parameter per row, and each row contains

1. a *key* that referrs to the parameter name,
2. the *value* that is used as constant for this parameter,
3. optionally a *help* string to give room for comments.


The full namespace of input parameters lets you define

- &cross; `test` of type string which defines if the run is a test run, accepted values are:
  a. `T` if yes,
  b. `F` if not;
  - &cross; `online` of type string, accepted values are:
  a. `T` if the script should run as online analysis,
  b. `F` if this value is not known,
  defaults to `F`, for details see the section *Modes of Operation* in *Additional Information*;
- &cross; `wavelength` of type float which defines the wavelength of probing light in nm;
- &cross; `substance` of type string which gives the name of the substance which the fluid is composed of;
- &cross; `substance_state` of type string which defines the state of the substance;
- &cross; `TCC_reference` of type string which defines the file containing a reference image that clarifies the location of TCC, the filename is first understood as relative path with root `pyridi/` and in case of no success as absolute path;
- &cross; `ionization_state` of type float, defines the ionization state of the probed matter, with zero for neutral matter;
- &cross; `know_magnification` of type string, accepted values are:
  a. `T` if the numerical value of the magnification of the imaging system or the space-per-pixel ratio is known,
  b. `F` if this value is not known;
- &cross; `pixel_calib` of type float which defines the space per pixel in units of um/pix imaged by the imaging system, if this value is not known then the parameters `magnification`, `detector_width`, and `detector_height` are needed for cases of `know_magnification == T`;
- &cross; `magnification` of type float which defines the magnification of the imaging system, needed only if value of `pixel_calib` is not known;
- &cross; `detector_width` of type float which defines the horizontal extend of the detector in the imaging system in mm, needed only if value of `pixel_calib` is not known;
- &cross; `detector_height` of type float which defines the vertical extend of the detector in the imaging system in mm, needed only if value of `pixel_calib` is not known;
- &cross; `calibration_N` (where N has to be replaced by a number) of type string which defines the file containing a calibration image, the filename is first understood as relative path with root `pyridi/` and in case of no success as absolute path, needed only if `know_magnification == F`;
- &cross; `delta_x_N_M` of type float which defines the difference between the spatial reference `calibration\_N` and `calibration\_M` (where N and M are numbers) along the horizontal x coordinate in mm, needed only if `know_magnification == F`;
- &cross; `delta_y_N_M` of type float which defines the difference between the spatial reference `calibration\_N` and `calibration\_M` (where N and M are numbers) along the vertical y coordinate in mm, needed only if `know_magnification == F`;
- &cross; `data` of type string which defines the directory containing data files or a specific data file, the path is first understood as relative path with root `pyridi/` and in case of no success as absolute path;
- &cross; `reference` of type string which defines a specific reference file, the path is first understood as relative path with root `pyridi/` and in case of no success as absolute path;
- &cross; `channel` of type string which defines the detector channel which should be used for analysis, accepted values are:
  a. `R` the red channel,
  b. `G` the green channel,
  c. `B` the blue channel,
  d. `A` the average of all available channels.
- &cross; `output_directory` of type string which defines the directory for output files, the path is understood as absolute path and defaults to `usr/out`;

---

# Milestones

- &check; setup project repository
- &cross; create test environment towards runtime test
