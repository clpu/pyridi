# Collection of Scripts and Spreadsheets

This directory groups tools usefull for interferometry.

## Nomarski Interferometer

For back on the enveloppe calculations towards a new Nomarski interferometer, you may use (and improove) `nomarski.ods`.

## Analysis Control

A browser page for the control of the automatic steps of the analysis is available with `analysis_control.html`.